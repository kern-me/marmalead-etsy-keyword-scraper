-- VARIABLES

property keyword : "q"

property scoreEtsySearches : "sumSearch"

property scoreEtsyEngagement : "sumEngagement"

property scoreCompetition : "sumCompetition"

property scoreCompeting : "totShops"

property scoreAvgRenewal : "avgRenewal"

property scoreBargainPrice : "minPrice"

property scoreMidrangePrice : "avgPrice"

property scorePremiumPrice : "maxPrice"

property defaultDelayValue : 0.75

property browserTimeoutValue : 60

property repeatCount : 19

-- ========================================
-- Copy the first cell of the spreadsheet
-- ========================================
on copyData() -- Copy the first cell of data
	tell application "System Events"
		delay defaultDelayValue
		log "Copy the first cell."
		keystroke "c" using command down
		delay defaultDelayValue
		log "Arrow to the right."
		key code 124 -- Arrow Right
		delay defaultDelayValue
		log "First cell of Google Sheets is Copied!"
	end tell
end copyData

-- ========================================
-- Paste the clipboard and right arrow
-- ========================================
on pasteValues()
	tell application "System Events"
		delay defaultDelayValue
		log "Pasting the clipboard to Google Sheets"
		keystroke "v" using command down
		delay defaultDelayValue
		log "Arrow right"
		key code 124 -- Arrow Right
		delay defaultDelayValue
	end tell
end pasteValues

-- =======================================
-- Make a new row in Google Sheets
-- =======================================
on nextRow()
	tell application "System Events"
		delay defaultDelayValue
		key code 125 -- Arrow Down
		log "Arrow Down"
		delay defaultDelayValue
		key code 115 -- Back to the First Cell
		log "Home Key to go back to the first cell of next row"
		delay defaultDelayValue
	end tell
end nextRow

-- ========================================
-- Find the Marmalead search bar in the DOM
-- and paste to the clipboard
-- ========================================
on inputByID(theId)
	tell application "Safari"
		activate
		do JavaScript "document.getElementById('" & theId & "').value ='" & (the clipboard) & "'; doSearch();" in document 1
		delay defaultDelayValue
		log "Pasted the clipboard into the search input."
		log "Activated the search! doSearch()"
	end tell
end inputByID

-- =======================================
-- Grab data from specific IDs in the DOM
-- =======================================
on getInputByClass(theId, doStrip)
	tell application "Safari"
		activate
		delay defaultDelayValue
		log "Find the DOM node"
		if doStrip = 1 then
			log "doStrip === " & doStrip & ""
			log "Strip the String."
			delay defaultDelayValue
			set input to do JavaScript "document.getElementById('" & theId & "').innerHTML.replace('k','000').replace('k','000').split(' - ',1).toString();" in document 1
			delay defaultDelayValue
		else if doStrip = 0 then
			log "doStrip === " & doStrip & ""
			log "Don't Strip String"
			delay defaultDelayValue
			set input to do JavaScript "document.getElementById('" & theId & "').innerHTML;" in document 1
			delay defaultDelayValue
		end if
		delay defaultDelayValue
		return input
	end tell
end getInputByClass

-- =======================================
-- Check the browser to make sure the DOM
-- is loaded
-- =======================================
on checkIfLoaded()
	set browserTimeoutValue to 60
	tell application "Safari"
		delay 1
		repeat with i from 1 to the browserTimeoutValue
			tell application "Safari"
				delay 2
				log "Checking if the loading screen is still active."
				set checkLoading to (do JavaScript "document.getElementById('loading').style.display;" in document 1)
				set loggedOut to (do JavaScript "document.getElementById('loError').style.display;" in document 1)
				delay 2
				if checkLoading is "none" then
					log "Results loaded!"
					return "Results loaded!"
				else if i is the browserTimeoutValue then
					return "Timed out! Stopping."
				else
					log "Still loading..."
				end if
			end tell
		end repeat
	end tell
end checkIfLoaded

-- =======================================
-- Check if the keyword is found
-- =======================================
on checkKeyword()
	tell application "Safari"
		delay 0.5
		log "Checking if there are results for the keyword."
		set noResultsCheck to (do JavaScript "document.getElementById('noresults').style.display;" in document 1)
		if noResultsCheck is "none" then
			log "Results are found! Let's keep going."
			return "Results Found"
		else
			log "No Results Were Found."
		end if
		return "no results"
	end tell
end checkKeyword

on recordTheData(theData)
	delay defaultDelayValue
	set the clipboard to theData
	delay defaultDelayValue
	pasteValues()
	end recordTheData

-- =======================================
-- Get the Etsy Data
-- =======================================
on getEtsyData()
	set EtsySearches to getInputByClass(scoreEtsySearches, 1)
	log EtsySearches
	
	delay defaultDelayValue
	
	set EtsyEngagement to getInputByClass(scoreEtsyEngagement, 1)
	log EtsyEngagement
	
	delay defaultDelayValue
	
	set Competition to getInputByClass(scoreCompetition, 1)
	log Competition
	
	delay defaultDelayValue
	
	set Competing to getInputByClass(scoreCompeting, 0)
	log Competing
	
	delay defaultDelayValue
	
	set AvgRenewal to getInputByClass(scoreAvgRenewal, 0)
	log AvgRenewal
	
	delay defaultDelayValue
	
	set BargainPrice to getInputByClass(scoreBargainPrice, 0)
	log BargainPrice
	
	delay defaultDelayValue
	
	set MidrangePrice to getInputByClass(scoreMidrangePrice, 0)
	log MidrangePrice
	
	delay defaultDelayValue
	
	set PremiumPrice to getInputByClass(scorePremiumPrice, 0)
	log PremiumPrice
	
	delay defaultDelayValue
	
	log "Done grabbing data!"
	
	tell application "Google Chrome" to activate
	-- Set clipboard to each variable and paste them into the spreadsheet
	
	recordTheData(EtsySearches)
	recordTheData(EtsyEngagement)
	recordTheData(Competition)
	recordTheData(Competing)
	recordTheData(AvgRenewal)
	recordTheData(BargainPrice)
	recordTheData(MidrangePrice)
	recordTheData(PremiumPrice)
	
	nextRow()
end getEtsyData



-- =======================================
-- Primary Sequence Handler
-- =======================================
on grabDataFromList()
	repeat 1000 times
		log "Step 1/5"
		tell application "Google Chrome" to activate
		copyData()
		log "Step 2/5"
		tell application "Safari" to activate
		inputByID("q")
		log "Step 3/5"
		checkIfLoaded()
		log "Step 4/5"
		if checkKeyword() is "no results" then
			log "No results were found. going back to step 1..."
			tell application "Google Chrome" to activate
			set the clipboard to "No Results Found"
			pasteValues()
			nextRow()
		else
			log "Step 5/5"
			getEtsyData()
		end if
	end repeat
end grabDataFromList


-- =======================================
-- Handler Calls
-- =======================================
grabDataFromList()

